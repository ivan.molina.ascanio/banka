# Banka

   Tutorial del uso del aplicativo banka,para crear transferencias,bancarias y cuentas

## Como correr el proyecto Android
* Clonar este repositorio.
* Abrir Terminal en el S.O de preferencia
* Correr comando "npm install" para poder instalar librerias por defecto
* ionic cordova build android
* ionic cordova run android



## Como correr el proyecto IOS
* Clonar este repositorio.
* Abrir Terminal en el S.O de preferencia
* Correr comando "npm install" para poder instalar librerias por defecto
* ionic cordova build ios
* ionic cordova run ios


## Tecnologias Usadas
    Nodejs:12.22.0
    Angular:8
    IONIC:v4.5

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckLoginGuard } from './guards/check-login.guard';
import { GuestGuard } from './guards/guest.guard';

const routes: Routes = [
  { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule',canActivate:[CheckLoginGuard] },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule',canActivate:[GuestGuard] },
  { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' ,canActivate:[GuestGuard] },
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' ,canActivate:[CheckLoginGuard] },
  { path: 'disclaimer', loadChildren: './pages/disclaimer/disclaimer.module#DisclaimerPageModule',canActivate:[CheckLoginGuard] },
  { path: 'information', loadChildren: './pages/information/information.module#InformationPageModule',canActivate:[CheckLoginGuard]  },
  { path: 'creacioncuenta', loadChildren: './pages/creacioncuenta/creacioncuenta.module#CreacioncuentaPageModule',canActivate:[CheckLoginGuard]  },
  { path: 'transferencias', loadChildren: './pages/transferencias/transferencias.module#TransferenciasPageModule',canActivate:[CheckLoginGuard]  },
  { path: 'cuentasterceros', loadChildren: './pages/cuentasterceros/cuentasterceros.module#CuentastercerosPageModule' ,canActivate:[CheckLoginGuard] }




];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

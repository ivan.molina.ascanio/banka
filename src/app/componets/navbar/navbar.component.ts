import { Component, Input, OnInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Input()name;
  constructor(private statusBar:StatusBar,private navCtrl:NavController) { 
    this.statusBar.backgroundColorByHexString('#dc3545');

  }

  ngOnInit() {}

  close(){
    this.navCtrl.pop()

  }

}

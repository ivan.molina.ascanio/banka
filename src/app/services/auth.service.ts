import { Injectable } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private alertController:AlertController,private auth:AngularFireAuth,private navCtrl:NavController,private db:AngularFirestore) { }
  registrarUsuario(us,pass,datos){
    this.auth.auth.createUserWithEmailAndPassword(us,pass).then(res=>{
    this.db.collection('usuarios').doc(res.user.uid).set(datos).then(()=>{
      console.log('usuario registrado');
      this.navCtrl.navigateForward(['/disclaimer'])
    })
  })
  }
  loginUsuario(us,pass){
    try{
      this.auth.auth.signInWithEmailAndPassword(us,pass.toString()).then(res=>{
        this.presentAlert('Session Correcta','Bienvenido a BANKA').then(()=>{
          this.navCtrl.navigateForward(['/tabs/tab1'])
        })
      }).catch(e=>{
        if(e.code=='auth/wrong-password'){
          this.presentAlert('Error',"Contraseña Incorrecta Porfavor Verifique Su contraseña")
        }else{
          if(e.code=='auth/too-many-requests'){
            this.presentAlert("Acceso Bloqueado","Se ha bloqueado por seguridad, unos minutos el acceso a su cuenta,por tantos intentos invalidos de contraseña")
  
          }
        }
  
        if(e.code=='auth/user-not-found'){
          this.presentAlert("Cuenta inexistente","Esta cuenta no existe ")
  
        }
        
      })
    }catch(e){
      

    }
   
  }
  async presentAlert(sub,msg) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alerta',
      subHeader: sub,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();


    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

}

import { Injectable } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private navCtrl:NavController,private db:AngularFirestore,private alertController:AlertController) { }


 async storeAccount(datai){
  
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Desea Eligir Esta Opción',
        message: 'Recuerde que al registrar esta cuenta, tendra la posibilidad de cancelarla',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              this.navCtrl.navigateForward([''])
            }
          }, {
            text: 'Registrar',
            handler: () => {
              this.db.collection('accounts').add(datai).then(()=>{
                  this.presentAlert().then(()=>{
                    window.location.reload()
                  })
              })
            }
          }
        ]
      });
  
      await alert.present();
    
    
  }
  
  async storeAccountTer(data){
    this.verifiedNumberAccount(data.num_cuenta)
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Desea Registrar esta Cuenta',
      message: 'Recuerde que al registrar esta cuenta, podra hacer diferente operaciones',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.navCtrl.navigateForward(['']).then(()=>{
              window.location.reload()
            })
          }
        }, {
          text: 'Registrar',
          handler: () => {
            this.db.collection('terceros').add(data).then(()=>{
                this.presentAlertMsg("Perfecto",'Cuenta tercero creada')
            })
          }
        }
      ]
    });
    await alert.present()
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cuenta Creada Exitosamente',
      subHeader: '',
      message: 'Felicitaciones por el uso de su nueva cuenta',
      buttons: [ {
        text: 'Perfecto',
        handler: (blah) => {
          this.navCtrl.navigateForward([''])
        }
      }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
  async presentAlertMsg(title,msg) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header:title,
      subHeader: '',
      message: msg,
      buttons: [ {
        text: 'Perfecto',
        handler: (blah) => {
          this.navCtrl.navigateForward(['/']).then(()=>{
            window.location.reload()
          })
        }
      }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
  async presentAlertMsgV(title,msg) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header:title,
      subHeader: '',
      message: msg,
      buttons: [ {
        text: 'Perfecto',
        handler: (blah) => {
         window.location.reload()
        }
      }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
  async verifiedNumberAccount(cod){
    this.db.collection('terceros',ref=>ref.where('num_cuenta','==',cod)).get().subscribe(l=>{
      l.forEach(o=>{
        if(o.exists){
          this.presentAlertMsgV('Error!',`Este Número de cuenta asociado tercero Existe: ALIAS_${o.data()['alias'].toUpperCase()}`)
        }
      })
    })
  }
} 

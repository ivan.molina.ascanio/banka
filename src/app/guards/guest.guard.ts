import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {
  constructor(private afAuth:AngularFireAuth,private router:Router){}
  canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.afAuth.authState.pipe(map(auth => {
		if (auth==null || auth==undefined) {
      return true;
		} else {
      this.router.navigate(['/tabs/principal']);
			return false;
		}
		}));
    }
  
}

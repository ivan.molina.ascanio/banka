import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from 'src/app/services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  nameFormControl: FormControl;
  typeidentificationFormControl: FormControl;
  identificationFormControl: FormControl;
  emailFormControl: FormControl;
  passwordFormControl: FormControl;
  nombreFormControl: FormControl;
  tipodocumentoFormControl: FormControl;
  documentoFormControl: FormControl;
  direccionresidencialFormControl: FormControl;

  constructor(private _snackBar: MatSnackBar,private auth:AuthService) { 
    this.emailFormControl = new FormControl('', [Validators.required, Validators.email]);
    this.passwordFormControl = new FormControl('', [Validators.required,Validators.min(8)])
    this.nombreFormControl = new FormControl('', [Validators.required]);
    this.tipodocumentoFormControl = new FormControl('', [Validators.required])
    this.documentoFormControl = new FormControl('', [Validators.required,Validators.min(8)])
    this.direccionresidencialFormControl = new FormControl('', [Validators.required])
 
  }

  ngOnInit() {
  }
  registrarUsuario(){
    var datos={
      nombre:this.nombreFormControl.value,
      tipodocumento:this.tipodocumentoFormControl.value,
      documento:this.documentoFormControl.value,
      direccionresidencial:this.direccionresidencialFormControl.value,
   }
   
    this.auth.registrarUsuario(this.emailFormControl.value,this.passwordFormControl.value.toString(),datos)

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });

    
  }
}

import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-creacioncuenta',
  templateUrl: './creacioncuenta.page.html',
  styleUrls: ['./creacioncuenta.page.scss'],
})
export class CreacioncuentaPage implements OnInit {
  slideOptsOne
  loader=0
  constructor(private datai:DataService,private auth:AngularFireAuth) {
   this.slideOptsOne = {
      initialSlide: 0
     
     };
   }

  ngOnInit() {
  }

 /* Creating a new account with the type of corriente. */
  creacionCorriente(){
    this.loader=1

    let code = Math.floor(Math.random() * 1000000);

  this.auth.auth.onAuthStateChanged(f=>{
    var data={
      uid:f.uid,
      codigo:code,
      saldo:0,
      active:true,
      type:'corriente',
      startDate:new Date(),
      divisa:'COP',
      te:0.8,
      imp:false,
      transfers:[]
    }
    console.log(data)
    this.datai.storeAccount(data)

  })
   
  }

  /* Creating a new account with the type of ahorros. */
  creacionAhorros(){
    this.loader=1
    let code = Math.floor(Math.random() * 1000000);

    this.auth.auth.onAuthStateChanged(f=>{
      var data={
        uid:f.uid,
        codigo:code,
        saldo:0,
        active:true,
        type:'ahorros',
        startDate:new Date(),
        divisa:'COP',
        te:0.4,
        imp:true,
        transfers:[]
      }
      console.log(data)
      this.datai.storeAccount(data)

    })
     
  }

}

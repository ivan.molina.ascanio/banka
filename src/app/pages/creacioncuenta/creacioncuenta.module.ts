import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreacioncuentaPage } from './creacioncuenta.page';
import { ComponetsModule } from 'src/app/componets/componets.module';

const routes: Routes = [
  {
    path: '',
    component: CreacioncuentaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponetsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreacioncuentaPage]
})
export class CreacioncuentaPageModule {}

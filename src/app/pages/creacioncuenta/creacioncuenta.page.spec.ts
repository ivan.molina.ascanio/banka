import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreacioncuentaPage } from './creacioncuenta.page';

describe('CreacioncuentaPage', () => {
  let component: CreacioncuentaPage;
  let fixture: ComponentFixture<CreacioncuentaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreacioncuentaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreacioncuentaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

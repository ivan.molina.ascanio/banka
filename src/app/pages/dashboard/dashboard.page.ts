import { Component, OnInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  public nombre
  public documento
  public correo
  public foto='assets/img/profile.png'
  public accounts
  constructor(private auth:AngularFireAuth,private db:AngularFirestore,private statusBar:StatusBar,private navCtrl:NavController) {
    this.traerInformacion()
    this.traerCuentas()
   }

  ngOnInit() {
  }

/* A function that is called when the user clicks on the button "Crear Cuenta" */
  gotoBiller(){
    this.navCtrl.navigateForward(['/creacioncuenta'])
  }

/* Getting the user information from the database. */
  traerInformacion(){
    this.auth.auth.onAuthStateChanged(u=>{
       this.db.collection('usuarios').doc(u.uid).get().subscribe(i=>{
          this.nombre=i.data()['nombre']
          this.documento=i.data()['documento']
       })
    })
    
  }
/* A function that is used to format the value of the account. */
  formatCOPValue(value) {
    let format = 'COP $ ';
    if (value)
        return (format + value.toLocaleString('en-US'));

    return `COP 0`;
 }
 gotoTerceros(){
  this.navCtrl.navigateForward(['/cuentasterceros'])

 }
/* A function that is called when the user clicks on the button "Transferir". */
 gotoTransfer(){
  this.navCtrl.navigateForward(['/transferencias'])

 }
/* A function that is used to get the accounts of the user. */
  traerCuentas(){
    this.accounts=[]
    this.auth.auth.onAuthStateChanged(u=>{
      this.db.collection('accounts',ref=>ref.where('uid','==',u.uid)).get().subscribe(i=>{
         i.forEach(j=>{
          
           this.accounts.push(j.data());
         })
      })
   })
   
  }
  information(object){
    this.navCtrl.navigateForward(['/seed-point'],{
      queryParams: {
          'details': JSON.stringify(object)
        }
      })
  }
}

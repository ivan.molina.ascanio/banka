import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentastercerosPage } from './cuentasterceros.page';

describe('CuentastercerosPage', () => {
  let component: CuentastercerosPage;
  let fixture: ComponentFixture<CuentastercerosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentastercerosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentastercerosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

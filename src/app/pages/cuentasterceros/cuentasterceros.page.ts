import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-cuentasterceros',
  templateUrl: './cuentasterceros.page.html',
  styleUrls: ['./cuentasterceros.page.scss'],
})
export class CuentastercerosPage implements OnInit {
  public aliasFormControl: FormControl;
  public entidad: FormControl;
  public tipo: FormControl;
  public documento: FormControl;
  public numcuenta:FormControl;
  public moneda:FormControl;

  public number = /^[0-9]{7,11}$/;
  public valueDocument = /^[0-9]{7,12}$/;
  public bancos
  constructor(private data:DataService,private http:HttpClient,private db:AngularFirestore,private auth:AngularFireAuth) { 
    this.bancos=[]
    
    this.aliasFormControl= new FormControl('', [Validators.required])
    this.entidad= new FormControl('', [Validators.required])
    this.tipo= new FormControl('', [Validators.required])
    this.documento= new FormControl('', [Validators.required,Validators.pattern(this.valueDocument)])
    this.moneda= new FormControl('', [Validators.required])
    this.numcuenta=new FormControl('', [Validators.required,Validators.pattern(this.number)])
    
    this.http.get('https://bogota-laburbano.opendatasoft.com/api/records/1.0/search/?dataset=bancos-de-colombia&q=',{}).subscribe(i=>{
     for(let k of i['records'] ) {
      console.log(k.fields.nombre)
      this.bancos.push(k.fields.nombre)

     }
   
    })
  }

  ngOnInit() {
  }
  creacionCuentaTerceros(){
    this.auth.auth.onAuthStateChanged(q=>{
      var json={
        alias:this.aliasFormControl.value,
        codigo:this.numcuenta.value,
        entidad_bank:this.entidad.value,
        tipo_cuenta:this.tipo.value,
        num_cuenta:this.numcuenta.value,
        documento:this.documento.value,
        moneda:this.moneda.value,
        asociador:q.uid
      }
      console.log(json)
      this.data.storeAccountTer(json)    
    })
  
  }
/*  */
 validate(){
  if(this.aliasFormControl.invalid && this.entidad.invalid && 
    this.tipo.invalid
    && this.documento.invalid && this.moneda.invalid && this.numcuenta.invalid) {
      document.getElementById('botonSave').setAttribute('disabled','disabled')
   }else{
    document.getElementById('botonSave').removeAttribute('disabled')

   }
 }
  /* A function that is used to verify the number of the account. */
  verifiedNumberAccount(){
    this.data.verifiedNumberAccount(this.numcuenta.value)
  }
 
}

import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { NavController, Platform } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { FieldPath } from 'angularfire2/firestore'; 
import * as firebase from 'firebase';
@Component({
  selector: 'app-transferencias',
  templateUrl: './transferencias.page.html',
  styleUrls: ['./transferencias.page.scss'],
})

export class TransferenciasPage implements OnInit {
  public opcion=0
  public moneda:FormControl
  public cuenta:FormControl
  public cuentaP:FormControl

  public monto:FormControl
  public accounts
  public accountsP
  public value
  public transfer
  public resultQR: string;
  constructor(private navCtrl:NavController,private qrScanner:QRScanner,private platform:Platform,private auth:AngularFireAuth,private db:AngularFirestore) {
       this.moneda=new FormControl('', [Validators.required])
       this.cuenta=new FormControl('', [Validators.required])
       this.cuentaP=new FormControl('')

       this.monto= new FormControl('', [Validators.required])
       this.traerCuentas()
       this.traerCuentasPropias()
   }

  ngOnInit() {
  }
  opQR(){
    this.opcion=1
  }
  creartransferencia(){

  }
  generarQR(){
    const id=Math.random().toString(36).substring(2);

    this.db.collection('transferencias').doc(id).set({
      moneda:this.moneda.value,
      cuentaOrigen:this.cuenta.value,
      monto:this.monto.value
    }).then(()=>{
      this.value=id
      this.opcion=3
    })
  }
  traerCuentas(){
    this.accounts=[]
    this.auth.auth.onAuthStateChanged(u=>{
      this.db.collection('accounts',ref=>ref.where('uid','==',u.uid)).get().subscribe(i=>{
         i.forEach(j=>{
           this.accounts.push(j.data())
         })
      })
   })
  }

/* A function that is called when the user clicks on the button to scan the QR code. */
  botonQR(){
    this.transfer=[]
    this.qrScanner.prepare().then((status:QRScannerStatus)=>{
      if(status.authorized){
         this.qrScanner.show()
     
         document.getElementsByTagName("body")[0].style.opacity="0";
         let scanSub=this.qrScanner.scan().subscribe((textFormatr)=>{
           this.resultQR=textFormatr
            this.db.collection('transferencias').doc(this.resultQR).get().subscribe(j=>{
               let monto=j.data()['monto']
               this.opcion=4
               if(j.data()['moneda']=='USD'){
                    monto=monto*3959
               }
         
               this.transfer.push({montou:monto,cuenta:j.data()['cuentaOrigen']})
               
            })  

           document.getElementsByTagName("body")[0].style.opacity="1";
           scanSub.unsubscribe()

           this.platform.backButton.subscribeWithPriority(0,()=>{
            
             document.getElementsByTagName("body")[0].style.opacity="1"
             scanSub.unsubscribe()
           })
         },(err)=>{
         })
         
         
      }
  })
  }
  transferir(transact){
    console.log(this.cuentaP.value)
    this.auth.auth.onAuthStateChanged(f=>{
      this.db.collection('accounts',ref=>ref.where('codigo','==',parseInt(this.cuentaP.value))).get().subscribe(k=>{
        k.forEach(o=>{
            o.ref.update({
              saldo:firebase.firestore.FieldValue.increment(transact.montou),
              transfers:firebase.firestore.FieldValue.arrayUnion(o.data()['uid'])
            }).then(()=>{
              alert("Transacción Realizada con exito")
              this.navCtrl.navigateForward(['/']).then(()=>{
                  this.opcion=0
                  this.value=null
                  window.location.reload()
              })
            }).catch((e)=>{
              console.log(e)
            })
        })
      })
    })
  }
  terminarTransferencia(){
    this.value=null
    this.opcion=0
    this.navCtrl.navigateForward(['/'])
  }
  formatCOPValue(value) {
    let format = 'COP $ ';
    if (value)
        return (format + value.toLocaleString('en-US'));

    return `COP 0`;
 }
 traerCuentasPropias(){
  this.accountsP=[]
  this.auth.auth.onAuthStateChanged(u=>{
    this.db.collection('accounts',ref=>ref.where('uid','==',u.uid)).snapshotChanges().subscribe(i=>{
       i.forEach(j=>{
        
         this.accountsP.push(j.payload.doc.data());
         console.log(this.accountsP)
       })
    })
 })
}
}




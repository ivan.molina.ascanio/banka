import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  constructor(private auth:AngularFireAuth,private navCtrl:NavController) {

  }
  logout(){
    this.auth.auth.signOut().then(()=>{
      this.navCtrl.navigateForward(['/login']).then(()=>{
        window.location.reload()
      })
    })

  }
}
